extends Spatial

var movements = []
var current_movement = 0
onready var anim_player = get_node("Scene Root/AnimationPlayer")


func _ready():
	anim_player.get_animation("run_like_usain_bolt").set_loop(true)
	anim_player.play("idle")


func _process(delta):
	if movements.size() == 0:
		return
	
	if get_translation() != movements[current_movement][0] and \
		anim_player.get_current_animation() != "run_like_usain_bolt" and \
		current_movement != 0:
		anim_player.play("run_like_usain_bolt")
	elif get_translation() == movements[current_movement][0]:
		anim_player.play("idle")
	
	set_translation(movements[current_movement][0])
	set_rotation(Vector3(0, movements[current_movement][1], 0))	
	current_movement += 1
	if current_movement > movements.size() - 1:
		current_movement = 0
