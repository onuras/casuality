extends Spatial

func _ready():
	get_node("CharacterRTS").as = _create_as()


func _create_as():
	var as = AStar.new()
	
	as.add_point(1, Vector3(-0.634072, 0, -1.094085))
	as.add_point(2, Vector3(-0.609841, 0, 0.774761))
	as.add_point(3, Vector3(-0.440759, 0, 2.647499))
	as.connect_points(1, 2)
	as.connect_points(3, 2)
	
	as.add_point(4, Vector3(0.742274, 0, 0.636312))
	as.add_point(5, Vector3(2.708832, 0, 0.683181))
	as.add_point(6, Vector3(4.234173, 0, 0.526831))
	as.connect_points(2, 4)
	as.connect_points(4, 5)
	as.connect_points(5, 6)
	
	as.add_point(7, Vector3(6.126438, 0, -1.327263))
	as.add_point(8, Vector3(6.26021, 0, 0.668094))
	as.add_point(9, Vector3(6.169401, 0, 3.010022))
	as.connect_points(6, 8)
	as.connect_points(7, 8)
	as.connect_points(8, 9)
	
	as.add_point(10, Vector3(7.864702, 0, 1.000036))
	as.add_point(11, Vector3(9.723035, 0, 0.823798))
	as.add_point(12, Vector3(11.329519, 0, 0.708209))
	as.connect_points(8, 10)
	as.connect_points(10, 11)
	as.connect_points(11, 12)
	
	as.add_point(13, Vector3(12.939808, 0, -4.231094))
	as.add_point(14, Vector3(13.415635, 0, -2.697333))
	as.add_point(15, Vector3(13.324042, 0, -1.288071))
	as.add_point(16, Vector3(13.269068, 0, 0.550142))
	as.add_point(17, Vector3(13.318146, 0, 2.873126))
	as.add_point(18, Vector3(13.401679, 0, 4.46968))
	as.add_point(19, Vector3(13.158165, 0, 5.954612))
	as.connect_points(12, 16)
	as.connect_points(13, 14)
	as.connect_points(14, 15)
	as.connect_points(15, 16)
	as.connect_points(16, 17)
	as.connect_points(17, 18)
	as.connect_points(18, 19)
	
	return as