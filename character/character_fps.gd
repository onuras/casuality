extends KinematicBody

var MOTION_SPEED = 4
var MOUSE_SENSITIVITY = 1500

var _record_movement = false
var _movements = []


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _physics_process(delta):
	_move_with_input(delta)
	if _record_movement:
		_record_movement()


func _move_with_input(delta):
	var cam_xform = get_node("Camera").get_global_transform()
	
	var motion = Vector3()
	
	if Input.is_action_pressed("move_forward"):
		motion += -cam_xform.basis[2]
	if Input.is_action_pressed("move_backward"):
		motion += cam_xform.basis[2]
	if Input.is_action_pressed("strafe_left"):
		motion += -cam_xform.basis[0]
	if Input.is_action_pressed("strafe_right"):
		motion += cam_xform.basis[0]
	
	if motion:
		motion.y = 0
		move_and_slide(motion.normalized() * MOTION_SPEED, Vector3(0, 1, 0))


func _input(event):
	_debug_input(event)

	if event is InputEventMouseMotion:
		var relative = event.get_relative()
		rotate_y(-relative.x / MOUSE_SENSITIVITY)
		
		# Y rotation
		var current_rotation = get_node("Camera").get_rotation().x
		if (current_rotation > -1.5 and current_rotation < 1.5) \
			or (current_rotation <= -1.5 and -relative.y > 0) \
			or (current_rotation >= -1.5 and -relative.y < 0):
			get_node("Camera").rotate_x(-relative.y / MOUSE_SENSITIVITY)


func _debug_input(event):
	if not OS.has_feature("debug"):
		return
	
	if event.is_action_pressed("record_movement"):
		_record_movement = !_record_movement
		if _record_movement:
			get_node("Control/Label").set_text("Recording")
		else:
			get_node("Control/Label").set_text("")
		print("Record movement: ", _record_movement, " Size of movements: ", _movements.size())
	
	if event.is_action_pressed("play_movement"):
		print("Playing movements")
		get_node("Control/Label").set_text("Playing")
		_play_movement()


func _record_movement():
	_movements.append([get_translation(), get_rotation().y])

func _play_movement():
	var replay = preload("res://replay/replay.tscn").instance()
	replay.movements = _movements.duplicate()
	get_parent().add_child(replay)