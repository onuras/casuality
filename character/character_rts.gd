extends KinematicBody


const MOTION_SPEED = 4
onready var anim_player = get_node("Meshes/AnimationPlayer")
var target = null setget set_target
var set_rotation = false
var as
var path = []
var path_current_idx = 0

func _ready():
	anim_player.get_animation("run_like_usain_bolt").set_loop(true)
	anim_player.play("idle")


func _physics_process(delta):
	# Do nothing if there is no target
	if not target:
		return
	
	if as:
		_move_with_as(delta)
	else:
		_move_directly_with_click(delta)


func _move_with_as(delta):
	var character_translation = get_translation()
	var direction = path[path_current_idx] - character_translation
	direction.y = 0
	_move_to(direction, delta)
	
	if not set_rotation:
		get_node("Meshes").look_at(path[path_current_idx], Vector3(0, 1, 0))
		set_rotation = true
	
	if character_translation.distance_to(path[path_current_idx]) < 0.25:
		if path_current_idx < len(path) - 1:
			path_current_idx += 1
			set_rotation = false
		else:
			target = null
			set_rotation = false
			anim_player.play("idle")
	
	



func _move_directly_with_click(delta):
	var character_translation = get_translation()
	var direction = target - character_translation
	direction.y = 0
	_move_to(direction, delta)
	
	if not set_rotation:
		get_node("Meshes").look_at(target, Vector3(0, 1, 0))
		set_rotation = true
	
	# FIXME: Need to extend vector here
	# Character is stopping when it reaches target
	if character_translation.distance_to(target) < 0.25:
		target = null
		set_rotation = false
		anim_player.play("idle")


func _move_to(direction, delta):
	global_translate(direction.normalized() * MOTION_SPEED * delta)
	
	# Play run animation
	if not anim_player.is_playing():
		anim_player.play("run_like_usain_bolt")
	
	# Play walking sound from sound animation
	# TODO: This is not implemented for HatunRunner
	#if not anim_player_sound.is_playing():
	#	anim_player_sound.play("WalkingSound")
	


func set_target(t):
	target = t
	set_rotation = false
	
	if as:
		_calculate_path(t)


func _on_Ground_input_event( camera, event, click_position, click_normal, shape_idx ):
	if not event.is_action_pressed("click"):
		return
	
	set_target(click_position)

func _input(event):
	if OS.has_feature("debug") and event.is_action_pressed("show_character_translation"):
		print(get_translation())


func _calculate_path(target):
	var character_translation = get_translation()
	var character_point = as.get_closest_point(character_translation)
	var destination_point = as.get_closest_point(target)
	path = as.get_point_path(character_point, destination_point)
	path_current_idx = 0